<?php

abstract class Config {

	const SITENAME = "AidosEngine.local";
	const SECRET = "DGLJDG5";
	const ADDRESS = "http://engine.local";
	const ADM_NAME = "Aidos Batyrkulov";
	const ADM_EMAIL = "aidos_batyrkulov@mail.ru";
	
	const API_KEY = "DKEL39DL";
	
	const DB_HOST = "localhost";
	const DB_USER = "root";
	const DB_PASSWORD = "220297";
	const DB_NAME = "engine";
	const DB_PREFIX = "xyz_";
	const DB_SYM_QUERY = "?";
	
	const DIR_IMG = "/images/";
	const DIR_IMG_ARTICLES = "/images/articles/";
	const DIR_AVATAR = "/images/avatars/";
	const DIR_TMPL = "/home/aidos/php/engine.local/tmpl/";
	const DIR_EMAILS = "/home/aidos/php/engine.local/tmpl/emails/";
	
	const LAYOUT = "main";
	const FILE_MESSAGES = "/home/aidos/php/engine.local/text/messages.ini";
	
	const FORMAT_DATE = "%d.%m.%Y %H:%M:%S";
	
	const COUNT_ARTICLES_ON_PAGE = 3;
	const COUNT_SHOW_PAGES = 10;
	
	const MIN_SEARCH_LEN = 3;
	const LEN_SEARCH_RES = 255;
	
	const SEF_SUFFIX = ".html";
	
	const DEFAULT_AVATAR = "default.png";
	const MAX_SIZE_AVATAR = 51200;
}